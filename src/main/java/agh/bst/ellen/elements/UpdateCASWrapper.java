package agh.bst.ellen.elements;

import java.util.concurrent.atomic.AtomicStampedReference;

public class UpdateCASWrapper {

    private final AtomicStampedReference<Info> update;

    public UpdateCASWrapper(Update updateValue) {
        update = new AtomicStampedReference<Info>(updateValue.info, updateValue.state.ordinal());
    }

    public Update getCopy() {
        Update ret = new Update(Update.State.values()[this.update.getStamp()], this.update.getReference());
        return ret;
    }

    /**
     *
     * @param expect
     * @param update
     * @return true if successful
     */
    public boolean cas(Update expect, Update update) {
        //TODO check if not getting false negatives
        boolean done = this.update.compareAndSet(expect.info, update.info, expect.state.ordinal(), update.state.ordinal()); //atomic
        return done;
    }

}
