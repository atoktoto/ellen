package agh.bst.ellen;

import agh.bst.ellen.elements.DeleteInfo;
import agh.bst.ellen.elements.InsertInfo;
import agh.bst.ellen.elements.Node;
import agh.bst.ellen.elements.Update;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.*;

public class EllenTree implements Set<Integer> {
    private final Node root;

    public EllenTree() {
        Node left = new Node(Integer.MIN_VALUE, null, null);
        Node right = new Node(Integer.MAX_VALUE, null, null);

        root = new Node(Integer.MAX_VALUE, left, right, new Update(Update.State.Clean, null));
    }

    public static class SearchResult {
        final Node grandParent, parent, leaf;
        final Update parentUpdate, grandParentUpdate;
        public SearchResult(Node grandParent, Node parent, Node leaf, Update parentUpdate, Update grandParentUpdate) {
            this.grandParent = grandParent;
            this.parent = parent;
            this.leaf = leaf;
            this.parentUpdate = parentUpdate; //TODO make a defensive copy
            this.grandParentUpdate = grandParentUpdate;
        }
    }

    public SearchResult search(int key) {
        Node grandParent = null, parent = null, leaf = root;
        Update grandParentUpdate = null, parentUpdate = null;

        while(leaf.isInternal()) {
            grandParent = parent;
            parent = leaf;
            grandParentUpdate = parentUpdate;
            parentUpdate = parent.update.getCopy();
            if(key < leaf.key) {
                leaf = parent.left.get();
            } else {
                leaf = parent.right.get();
            }
        }

        return new SearchResult(grandParent, parent, leaf, parentUpdate, grandParentUpdate);
    }

    public Node find(int key) {
        Node iter = root;

        while(iter.left.get() != null) { //iter.isInternal()
            if(key < iter.key) {
                iter = iter.left.get();
            } else {
                iter = iter.right.get();
            }
        }
        if(iter.key == key) {
            return iter;
        } else {
            return null;
        }
    }

    public boolean insert(int key) {

        final Node newLeaf = new Node(key, null, null);

        while(true) {

            // inline search
            Node parent = null, foundLeaf = root;
            while(foundLeaf.isInternal()) {
                parent = foundLeaf;
                if(key < foundLeaf.key) {
                    foundLeaf = parent.left.get();
                } else {
                    foundLeaf = parent.right.get();
                }
            }

            if(foundLeaf.key == key) {
                return false;   // cannot insert duplicate key
            }

            Update parentUpdate = parent.update.getCopy();
            if(parentUpdate.state != Update.State.Clean)  {
                help(parentUpdate);
            } else {
                final Node newSibling = new Node(foundLeaf.key, null, null);
                final Node newInternal;

                if(newLeaf.key < newSibling.key) {
                    newInternal = new Node(Math.max(key, foundLeaf.key), newLeaf, newSibling, new Update(Update.State.Clean, null));
                } else {
                    newInternal = new Node(Math.max(key, foundLeaf.key), newSibling, newLeaf, new Update(Update.State.Clean, null));
                }

                final InsertInfo op = new InsertInfo(parent, foundLeaf, newInternal);
                final boolean result = parent.update.cas(parentUpdate, new Update(Update.State.IFlag, op));

                if(result) {
                    boolean r = helpInsert(op);
                    if(r) {
                        return true;
                    }
                } else {
                    Update update = parent.update.getCopy();
                    help(update);
                }
            }
        }
    }

    private boolean helpInsert(InsertInfo op) {
        boolean casChildOk = CAS_child(op.parent, op.leaf, op.newInternal);
        boolean parentCasOk = op.parent.update.cas(new Update(Update.State.IFlag, op), new Update(Update.State.Clean, op));
        return casChildOk;
    }

    private boolean CAS_child(Node parent, Node old, Node newInternal) {
        if(newInternal.key < parent.key) {
            return parent.left.compareAndSet(old, newInternal);
//            UNSAFE.compareAndSwapObject(parent, nodeLeftChildOffset, old, newInternal);
        } else {
            return parent.right.compareAndSet(old, newInternal);
//            UNSAFE.compareAndSwapObject(parent, nodeRightChildOffset, old, newInternal);
        }
    }

    private void help(Update update) {
        if(update.state == Update.State.IFlag) {
            InsertInfo ii = (InsertInfo)update.info;
            helpInsert(ii);
        } else if(update.state == Update.State.Mark) {
            helpMarked((DeleteInfo)update.info);
        } else if(update.state == Update.State.DFlag) {
            helpDelete((DeleteInfo)update.info);
        }
    }

    public boolean delete(int k) {
        while(true){
            SearchResult searchResult = search(k);
            if(searchResult.leaf.key != k) return false;
            if(searchResult.grandParentUpdate.state != Update.State.Clean) {
                help(searchResult.grandParentUpdate);
            } else if(searchResult.parentUpdate.state != Update.State.Clean) {
                help(searchResult.parentUpdate);
            } else {
                DeleteInfo op = new DeleteInfo(searchResult.grandParent, searchResult.parent, searchResult.leaf, searchResult.parentUpdate);
                boolean result = searchResult.grandParent.update.cas(searchResult.grandParentUpdate, new Update(Update.State.DFlag, op));
                if(result) {
                    if(helpDelete(op)) return true;
                } else {
                    Update update = searchResult.grandParent.update.getCopy();
                    help(update);
                }
            }
        }
    }

    private boolean helpDelete(DeleteInfo op) {
        boolean result = op.parent.update.cas(op.parentUpdate, new Update(Update.State.Mark, op));
        if(result == true) {
            helpMarked(op);
            return true;
        } else {
            Update update = op.parent.update.getCopy();
            help(update);


                op.grandParent.update.cas(new Update(Update.State.DFlag, op), new Update(Update.State.Clean, op));


            return false;
        }
    }

    private void helpMarked(DeleteInfo op) {
        Node other;
        if(op.parent.right.get() == op.leaf) {
            other = op.parent.left.get();
        } else {
            other = op.parent.right.get();
        }


            CAS_child(op.grandParent, op.parent, other);
            op.grandParent.update.cas(new Update(Update.State.DFlag, op), new Update(Update.State.Clean, op));

    }

    private static final sun.misc.Unsafe UNSAFE;
    private static final long nodeLeftChildOffset;
    private static final long nodeRightChildOffset;
    static {
        try {
            Field singletonInstanceField = Unsafe.class.getDeclaredField("theUnsafe");
            singletonInstanceField.setAccessible(true);
            UNSAFE = (Unsafe) singletonInstanceField.get(null);
            Class k = Node.class;
            nodeLeftChildOffset = UNSAFE.objectFieldOffset(k.getDeclaredField("left"));
            nodeRightChildOffset = UNSAFE.objectFieldOffset(k.getDeclaredField("right"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public void print() {
        Utils.print(root);
    }

    public String toString() {
        return root.toString();
    }

    @Override
    public int size() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean isEmpty() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean contains(Object o) {
        return find((Integer)o) != null;
    }

    @Override
    public Iterator<Integer> iterator() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Object[] toArray() {
        ArrayList<Integer> ret = new ArrayList<Integer>();

        Stack<Node> stack = new Stack<Node>();
        stack.push(root);

        while (stack.size() > 0) {
            Node node = stack.pop();
            if(node.isInternal()) {
                stack.push(node.left.get());
                stack.push(node.right.get());
            } else {
                ret.add(node.key);
            }
        }

        return ret.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean add(Integer integer) {
        return insert(integer);
    }

    @Override
    public boolean remove(Object o) {
        return delete((Integer)o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public void clear() {
        throw new RuntimeException("Not implemented");
    }



}
