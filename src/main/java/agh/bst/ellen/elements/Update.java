package agh.bst.ellen.elements;

public class Update {

    public static enum State {
        Clean, DFlag, IFlag, Mark
    }

    public final State state;
    public final Info info;

    public Update(State state, Info info) {
        this.state = state;
        this.info = info;
    }

    public boolean equals(Update other ) {
        return this.state == other.state && this.info == other.info;
    }


}
