package agh.bst.ellen.elements;

public class UpdateCASWrapperSynchronised {
    private volatile Update update;

    public UpdateCASWrapperSynchronised(Update updateValue) {
        update = updateValue;
    }

    public synchronized Update getCopy() {
        return update;
    }

    /**
     *
     * @param expect
     * @param update
     * @return true if successful
     */
    public synchronized boolean cas(Update expect, Update update) {
        if(this.update.state == expect.state &&
                this.update.info == expect.info) {
            this.update = update;
            return true;
        } else {
            return false;
        }
    }
}
