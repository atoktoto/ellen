package agh.bst.ellen.elements;

public class InsertInfo extends Info {

    public final Node parent;
    public final Node newInternal;
    public final Node leaf;

    public InsertInfo(Node parent, Node leaf, Node newInternal) {
        this.parent = parent;
        this.newInternal = newInternal;
        this.leaf = leaf;
    }
}
