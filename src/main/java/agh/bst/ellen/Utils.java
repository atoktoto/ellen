package agh.bst.ellen;


import agh.bst.ellen.elements.Node;

public class Utils {

    public static void print(Node node) {
        StringBuilder builder = new StringBuilder();
        print(node, builder, "", true, true);
        System.out.print(builder.toString());
    }

    private static void print(Node node, StringBuilder builder, String prefix, boolean isTail, boolean root) {
        if(root) {
            builder.append(prefix + (isTail ? "o── " : "├── ") + node.key + "\n");
        } else {
            builder.append(prefix + (isTail ? "└── " : "├── ") + node.key + "\n");
        }
        if(node.left.get() != null && node.right.get() != null) {
            print(node.left.get(), builder, prefix + (isTail ? "    " : "│   "), false, false);
            print(node.right.get(), builder, prefix + (isTail ? "    " : "│   "), true, false);
        }
    }
}
