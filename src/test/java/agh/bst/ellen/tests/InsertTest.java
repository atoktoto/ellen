package agh.bst.ellen.tests;

import static org.junit.Assert.*;

import agh.bst.ellen.EllenTree;
import agh.bst.ellen.testing.TestUtils;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.carrotsearch.junitbenchmarks.annotation.AxisRange;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkHistoryChart;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkMethodChart;
import com.carrotsearch.junitbenchmarks.annotation.LabelType;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.*;
import org.junit.rules.TestRule;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static agh.bst.ellen.testing.TestUtils.*;

@AxisRange(min = 0, max = 1)
@BenchmarkMethodChart(filePrefix = "benchmark-insert")
@BenchmarkHistoryChart(labelWith = LabelType.CUSTOM_KEY, maxRuns = 20)
public class InsertTest {

    @Rule
    public TestRule benchmarkRun = new BenchmarkRule();

    static List<List<Integer>> randomIntGroups;
    static Set<Integer> shouldBe;
    static int threadPoolSize;

    @BeforeClass
    public static void prepare() {
        threadPoolSize = 4;
        randomIntGroups = TestUtils.getRandomInts(4, 100000, Integer.MAX_VALUE);
        shouldBe = new TreeSet<Integer>();
        for(List<Integer> ints : randomIntGroups) {
            for(int i : ints) {
                shouldBe.add(i);
            }
        }
        shouldBe = Collections.unmodifiableSet(shouldBe);
    }

    private ExecutorService getNewExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    // http://java.dzone.com/articles/gradle-goodness-running-single
    // http://stackoverflow.com/questions/21608240/performance-difference-between-passing-interface-and-class-reloaded

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void ellenTree_Insert() {
        final EllenTree tree = new EllenTree();
        insertIntegersWithThreads(randomIntGroups, getNewExecutor(), tree);

        Set<Integer> resultingSet = TestUtils.toTreeSet(tree);
        resultingSet.remove(-2147483648);
        resultingSet.remove(2147483647);

        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIteratorSpecial(tree, resultingSet, shouldBe));
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void ellenTree_sameThread_Insert() {
        final EllenTree tree = new EllenTree();
        insertIntegersWithThreads(randomIntGroups, MoreExecutors.sameThreadExecutor(), tree);

        Set<Integer> resultingSet = TestUtils.toTreeSet(tree);
        resultingSet.remove(-2147483648);
        resultingSet.remove(2147483647);

        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(resultingSet, shouldBe));
    }

    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 2)
    @Test public void sameThread_Insert() {
        final Set<Integer> treeSet = new TreeSet<Integer>();
        insertIntegersWithThreads(randomIntGroups, MoreExecutors.sameThreadExecutor(), treeSet);
        assertTrue("Should have same element count", TestUtils.compareSize(treeSet, shouldBe));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(treeSet, shouldBe));
    }


    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 2)
    @Test public void synchronizedHashSet_Insert() {
        final Set<Integer> synchronizedSet = Collections.synchronizedSet(new HashSet<Integer>());
        insertIntegersWithThreads(randomIntGroups, getNewExecutor(), synchronizedSet);
        assertTrue("Should have same element count", TestUtils.compareSize(synchronizedSet, shouldBe));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(synchronizedSet, shouldBe));
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void concurrentSkipList_Insert() {
        final ConcurrentSkipListSet<Integer> skipListSet = new ConcurrentSkipListSet<Integer>();
        insertIntegersWithThreads(randomIntGroups, getNewExecutor(), skipListSet);
        assertTrue("Should have same element count", TestUtils.compareSize(skipListSet, shouldBe));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(skipListSet, shouldBe));
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void nullSet_Insert() {
        final NullSet<Integer> nullSet = new NullSet<Integer>();
        insertIntegersWithThreads(randomIntGroups, getNewExecutor(), nullSet);
        assertTrue("Should have same element count", TestUtils.compareSize(shouldBe, shouldBe));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(shouldBe, shouldBe));
    }
}
