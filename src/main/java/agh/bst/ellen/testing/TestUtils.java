package agh.bst.ellen.testing;


import agh.bst.ellen.EllenTree;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class TestUtils {

    public static List<List<Integer>> getRandomInts(int buckets, int itemsPerBucket, int bound) {
        Random random = new Random();

        List<List<Integer>> ret = new ArrayList<List<Integer>>();
        for(int i2 = 0; i2 < buckets; i2++) {
            List<Integer> bucket = new ArrayList<Integer>();
            ret.add(bucket);
            for(int i = 0; i < itemsPerBucket; i++) {
                bucket.add(random.nextInt(bound));
            }
        }

        return make2DListImmutable(ret);
    }

    public static List<List<Integer>> getRandomIntsSubset(List<List<Integer>> buckets, float part) {
        Random random = new Random();

        List<List<Integer>> ret = new ArrayList<List<Integer>>();

        for(int i2 = 0; i2 < buckets.size(); i2++) {
            List<Integer> bucket = buckets.get(i2);
            List<Integer> newBucket = new ArrayList<Integer>();

            for(int i = 0; i < (int)(bucket.size() * part); i++) {
                newBucket.add(bucket.get(random.nextInt(bucket.size())));
            }


            ret.add(newBucket);
        }

        return make2DListImmutable(ret);
    }

    public static <T> List<List<T>> make2DListImmutable(List<List<T>> source) {
        List<List<T>> temp = new ArrayList<List<T>>();
        for (List<T> list : source) {
            temp.add(Collections.unmodifiableList(list));
        }
        return Collections.unmodifiableList(temp);
    }


    public static boolean compareSize(Set<Integer> set1, Set<Integer> set2) {
        return set1.size() == set2.size();
    }

    public static Set<Integer> toTreeSet(Set<Integer> set1) {
        TreeSet<Integer> ret = new TreeSet<Integer>();
        Object[] objects = set1.toArray();
        for(Object o : objects) {
            Integer i = (Integer)o;
            ret.add(i);
        }
        return ret;
    }

    public static boolean compareElementsUsingIteratorSpecial(EllenTree tree, Set<Integer> set1, Set<Integer> controlSet) {
        int errorCnt = 0;

        for(Integer i : set1) {
            if(!controlSet.contains(i)) {
                System.out.println("Extra element: " + i);
                errorCnt++;
            }
        }

        for(Integer i : controlSet) {
            if(!set1.contains(i)) {
                System.out.println("Missing element: " + i);
                boolean found = tree.find(i) != null;
                System.out.println("found = " + found);
                errorCnt++;
            }
        }

        if(errorCnt != 0) {
            System.out.println("errorCnt = " + errorCnt);
        }

        return errorCnt == 0;
    }

    public static boolean compareElementsUsingIterator(Set<Integer> set1, Set<Integer> controlSet) {

        int errorCnt = 0;

        for(Integer i : set1) {
            if(!controlSet.contains(i)) {
                System.out.println("Extra element: " + i);
                errorCnt++;
            }
        }

        for(Integer i : controlSet) {
            if(!set1.contains(i)) {
                System.out.println("Missing element: " + i);
                errorCnt++;
            }
        }

        if(errorCnt != 0) {
            System.out.println("errorCnt = " + errorCnt);
        }

        return errorCnt == 0;
    }


    public static void deleteIntegersWithThreads(final List<List<Integer>> buckets, final ExecutorService executor, final Set<Integer> set) {
        Collection<Runnable> runnables = new ArrayList<Runnable>();

        for(final List<Integer> bucket : buckets) {
            runnables.add(new Runnable() {
                @Override
                public void run() {
                    for(Integer i : bucket) {
                        set.remove(i);
                    }
                }
            });
        }

        for(Runnable runnable : runnables) {
            executor.execute(runnable);
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void insertIntegersWithThreads(final List<List<Integer>> buckets, final ExecutorService executor, final Set<Integer> set) {
        Collection<Runnable> runnables = new ArrayList<Runnable>();

        for(final List<Integer> bucket : buckets) {
            runnables.add(new Runnable() {
                @Override
                public void run() {
                    for(Integer i : bucket) {
                        set.add(i);
                    }
                }
            });
        }

        for(Runnable runnable : runnables) {
            executor.execute(runnable);
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void searchIntegersWithThreads(final List<List<Integer>> buckets, final ExecutorService executor, final Set<Integer> set) {
        Collection<Runnable> runnables = new ArrayList<Runnable>();

        for(final List<Integer> bucket : buckets) {
            runnables.add(new Runnable() {
                @Override
                public void run() {
                    for(Integer i : bucket) {
                        set.contains(i);
                    }
                }
            });
        }

        for(Runnable runnable : runnables) {
            executor.execute(runnable);
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
