package agh.bst.ellen.elements;

import java.util.concurrent.atomic.AtomicReference;

public class Node {

    public final int key;
    public final AtomicReference<Node> left;
    public final AtomicReference<Node> right;

    public final UpdateCASWrapper update;
//    public final UpdateCASWrapperSynchronised update;

    public Node(int key, Node left, Node right, Update updateValue) {
        this.key = key;
        this.left = new AtomicReference<Node>(left);
        this.right = new AtomicReference<Node>(right);

        if(updateValue != null) {
            update =  new UpdateCASWrapper(updateValue);
        } else {
            update = new UpdateCASWrapper(new Update(Update.State.Clean, null));
        }
    }

    public Node(int key, Node left, Node right) {
        this(key, left, right, null);
    }

    public Node(int key) {
        this(key, null, null);
    }

    public boolean isInternal() {
        return left.get() != null && right.get() != null;
    }

    public String toString() {
        if(!isInternal()) {
            return "leaf:" + Integer.toString(key);
        } else {
            return "internal: " + Integer.toString(key);
        }
    }

}
