package agh.bst.ellen.elements;

public class DeleteInfo extends Info {

    public final Node grandParent, parent, leaf;
    public final Update parentUpdate; //NOT A POINTER

    public DeleteInfo(Node grandParent, Node parent, Node leaf, Update parentUpdate) {
        this.grandParent = grandParent;
        this.parent = parent;
        this.leaf = leaf;
        this.parentUpdate = parentUpdate;
    }
}
