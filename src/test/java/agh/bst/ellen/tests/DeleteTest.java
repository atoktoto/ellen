package agh.bst.ellen.tests;

import agh.bst.ellen.EllenTree;
import agh.bst.ellen.testing.TestUtils;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.carrotsearch.junitbenchmarks.annotation.AxisRange;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkHistoryChart;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkMethodChart;
import com.carrotsearch.junitbenchmarks.annotation.LabelType;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static agh.bst.ellen.testing.TestUtils.deleteIntegersWithThreads;
import static agh.bst.ellen.testing.TestUtils.insertIntegersWithThreads;
import static org.junit.Assert.assertTrue;

@AxisRange(min = 0, max = 1)
@BenchmarkMethodChart(filePrefix = "benchmark-delete")
@BenchmarkHistoryChart(labelWith = LabelType.CUSTOM_KEY, maxRuns = 20)
public class DeleteTest {

    @Rule
    public TestRule benchmarkRun = new BenchmarkRule();

    List<List<Integer>> randomInts;
    List<List<Integer>> randomIntsSubset;

    Set<Integer> shouldBeAfterInsert;
    Set<Integer> shouldBeAfterRemove;

    int threadPoolSize;

    @Before
    public void prepare() {
        threadPoolSize = 4;
        randomInts = TestUtils.getRandomInts(4, 100000, Integer.MAX_VALUE);
        TreeSet<Integer> set1 = new TreeSet<Integer>();
        for(List<Integer> ints : randomInts) {
            for(int i : ints) {
                set1.add(i);
            }
        }

        TreeSet<Integer> setCopy = new TreeSet<Integer>(set1);
        shouldBeAfterInsert = Collections.unmodifiableSet(setCopy);


        randomIntsSubset = TestUtils.getRandomIntsSubset(randomInts, 0.5f);
        for(int i = 0; i < randomIntsSubset.size(); i++) {
            List<Integer> ints = randomIntsSubset.get(i);
            set1.removeAll(ints);
            System.out.println(set1.size());
        }

        shouldBeAfterRemove = Collections.unmodifiableSet(set1);
    }

    private ExecutorService getNewExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    // http://java.dzone.com/articles/gradle-goodness-running-single
    // http://stackoverflow.com/questions/21608240/performance-difference-between-passing-interface-and-class-reloaded

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void ellenTree_Delete() {
        final EllenTree tree = new EllenTree();
        insertIntegersWithThreads(randomInts, getNewExecutor(), tree);
        deleteIntegersWithThreads(randomIntsSubset, getNewExecutor(), tree);

        Set<Integer> resultingSet = TestUtils.toTreeSet(tree);
        resultingSet.remove(-2147483648);
        resultingSet.remove(2147483647);

        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIteratorSpecial(tree, resultingSet, shouldBeAfterRemove));
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void ellenTree_sameThread_Delete() {
        final EllenTree tree = new EllenTree();
        insertIntegersWithThreads(randomInts, MoreExecutors.sameThreadExecutor(), tree);
        deleteIntegersWithThreads(randomIntsSubset, MoreExecutors.sameThreadExecutor(), tree);

        Set<Integer> resultingSet = TestUtils.toTreeSet(tree);
        resultingSet.remove(-2147483648);
        resultingSet.remove(2147483647);

        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIteratorSpecial(tree, resultingSet, shouldBeAfterRemove));
    }

    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 2)
    @Test public void sameThread_Insert() {
        final Set<Integer> treeSet = new TreeSet<Integer>();
        insertIntegersWithThreads(randomInts, MoreExecutors.sameThreadExecutor(), treeSet);
        assertTrue("Should have same element count", TestUtils.compareSize(treeSet, shouldBeAfterInsert));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(treeSet, shouldBeAfterInsert));
    }


    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 2)
    @Test public void synchronizedHashSet_Insert() {
        final Set<Integer> synchronizedSet = Collections.synchronizedSet(new HashSet<Integer>());
        insertIntegersWithThreads(randomInts, getNewExecutor(), synchronizedSet);
        assertTrue("Should have same element count", TestUtils.compareSize(synchronizedSet, shouldBeAfterInsert));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(synchronizedSet, shouldBeAfterInsert));
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void concurrentSkipList_Insert() {
        final ConcurrentSkipListSet<Integer> skipListSet = new ConcurrentSkipListSet<Integer>();
        insertIntegersWithThreads(randomInts, getNewExecutor(), skipListSet);
        assertTrue("Should have same element count", TestUtils.compareSize(skipListSet, shouldBeAfterInsert));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(skipListSet, shouldBeAfterInsert));
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void nullSet_Insert() {
        final NullSet<Integer> nullSet = new NullSet<Integer>();
        insertIntegersWithThreads(randomInts, getNewExecutor(), nullSet);
        assertTrue("Should have same element count", TestUtils.compareSize(shouldBeAfterInsert, shouldBeAfterInsert));
        assertTrue("Should contain same elements", TestUtils.compareElementsUsingIterator(shouldBeAfterInsert, shouldBeAfterInsert));
    }
}
