package agh.bst.ellen.tests;

import agh.bst.ellen.EllenTree;
import agh.bst.ellen.testing.TestUtils;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.carrotsearch.junitbenchmarks.annotation.AxisRange;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkHistoryChart;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkMethodChart;
import com.carrotsearch.junitbenchmarks.annotation.LabelType;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static agh.bst.ellen.testing.TestUtils.insertIntegersWithThreads;
import static agh.bst.ellen.testing.TestUtils.searchIntegersWithThreads;
import static org.junit.Assert.assertTrue;

@AxisRange(min = 0, max = 1)
@BenchmarkMethodChart(filePrefix = "benchmark-search")
@BenchmarkHistoryChart(labelWith = LabelType.CUSTOM_KEY, maxRuns = 20)
public class SearchTest {

    @Rule
    public TestRule benchmarkRun = new BenchmarkRule();

    static List<List<Integer>> randomIntGroups;
    static Set<Integer> shouldBe;
    static int threadPoolSize;

    static final EllenTree ellenTree = new EllenTree();
    static final Set<Integer> treeSet = new TreeSet<Integer>();
    static final Set<Integer> synchronizedSet = Collections.synchronizedSet(new HashSet<Integer>());
    static final ConcurrentSkipListSet<Integer> skipListSet = new ConcurrentSkipListSet<Integer>();

    @BeforeClass
    public static void prepare() {
        threadPoolSize = 4;
        randomIntGroups = TestUtils.getRandomInts(4, 100000, Integer.MAX_VALUE);
        shouldBe = new TreeSet<Integer>();
        for(List<Integer> ints : randomIntGroups) {
            for(int i : ints) {
                shouldBe.add(i);
            }
        }
        shouldBe = Collections.unmodifiableSet(shouldBe);

        insertIntegersWithThreads(randomIntGroups, getNewExecutor(), ellenTree);
        insertIntegersWithThreads(randomIntGroups, MoreExecutors.sameThreadExecutor(), treeSet);
        insertIntegersWithThreads(randomIntGroups, getNewExecutor(), synchronizedSet);
        insertIntegersWithThreads(randomIntGroups, getNewExecutor(), skipListSet);
    }

    private static ExecutorService getNewExecutor() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    // http://java.dzone.com/articles/gradle-goodness-running-single
    // http://stackoverflow.com/questions/21608240/performance-difference-between-passing-interface-and-class-reloaded

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void ellenTree_Search() {
        searchIntegersWithThreads(randomIntGroups, getNewExecutor(), ellenTree);
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void ellenTree_sameThread_Search() {
        searchIntegersWithThreads(randomIntGroups, MoreExecutors.sameThreadExecutor(), ellenTree);
    }

    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 2)
    @Test public void set_sameThread_Search() {
        searchIntegersWithThreads(randomIntGroups, MoreExecutors.sameThreadExecutor(), treeSet);
    }

    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 2)
    @Test public void synchronizedHashSet_Search() {
        searchIntegersWithThreads(randomIntGroups, getNewExecutor(), synchronizedSet);
    }

    @BenchmarkOptions(benchmarkRounds = 10, warmupRounds = 2)
    @Test public void concurrentSkipList_Search() {
        searchIntegersWithThreads(randomIntGroups, getNewExecutor(), skipListSet);
    }
}
