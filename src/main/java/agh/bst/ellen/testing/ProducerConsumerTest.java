package agh.bst.ellen.testing;

import agh.bst.ellen.EllenTree;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class ProducerConsumerTest {
    public static void main(String[] args) {
        final EllenTree testedSet = new EllenTree();
//        final Set<Integer> testedSet = Collections.synchronizedSet(new HashSet<Integer>());

        producerConsumerTest(testedSet, Integer.MAX_VALUE);
    }

    public static void producerConsumerTest(final Set testedSet, final int lastInt) {
        final AtomicInteger insertCount = new AtomicInteger(0);
        final AtomicInteger removeCount = new AtomicInteger(0);

        System.out.println("Testing " + testedSet.getClass().getSimpleName() + " with element limit: " + lastInt);

        final Thread adder = new Thread() {
            @Override
            public void run() {
                for(int i = 0; i < lastInt; i++) {
                    testedSet.add(i);
                    Thread.yield();

                    if(i % 1000 == 0) {
                        insertCount.incrementAndGet();
                    }
                }
                System.out.println("Insert finish");
            }
        };

        Thread remover = new Thread() {
            long lastTime = 0;

            @Override
            public void run() {
                int i = 0;
                lastTime = System.currentTimeMillis();

                while(i < lastInt) {
                    if(testedSet.contains(i)) {
                        testedSet.remove(i);
                        i++;

                        if(i % 1000 == 0) {
                            removeCount.incrementAndGet();
                        }

                        if(i % 1000000 == 0) {
                            int inserted = insertCount.get();
                            int removed = removeCount.get();
                            int ahead = (inserted - removed) * 1000;

                            long timeSpent = System.currentTimeMillis() - lastTime;
                            lastTime = System.currentTimeMillis();

                            float speed = 1000000 / (timeSpent / 1000f);
                            System.out.println("Iteration: " + i + " / " + lastInt + ", Speed: " + speed + " ops/s, insert ahead: " + ahead);
                        }
                    } else {
                        Thread.yield();
                    }
                }
                System.out.println("Remove finish");
            }
        };

        adder.start();
        remover.start();
    }
}

