package agh.bst.ellen.testing;

import agh.bst.ellen.EllenTree;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

public class SimpleInsertTest {

    static final Set<Integer> synchronizedHashSet = Collections.synchronizedSet(new HashSet<Integer>());
    static final Set<Integer> synchronizedTreeMap = Collections.synchronizedSet(new TreeSet<Integer>());
    static final Set<Integer> concurrentSkipList = new ConcurrentSkipListSet<Integer>();
    static final Set<Integer> ellenTree = new EllenTree();

    public static void main(String[] args) throws InterruptedException {
        System.gc();

        // 100000 200000 300000
        int items = 300000;

        Integer[] input = new Integer[items];
        Random random = new Random();
        for(int i = 0; i < items; i++) {
            input[i] = random.nextInt(Integer.MAX_VALUE);
        }

        Thread.sleep(5000);

        memoryTest(concurrentSkipList, input);
        System.gc();
        Thread.sleep(10000);
    }

    private static void memoryTest(Set testedSet, Integer[] elements) {
        System.out.println("Start");
        for (int i = 0; i < elements.length; i++) {
            testedSet.add(elements[i]);
        }
    }
}
